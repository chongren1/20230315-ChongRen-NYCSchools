package com.chongre.jpmc.nycschools.view.adapter

import androidx.recyclerview.widget.RecyclerView.ViewHolder
import com.chongre.jpmc.nycschools.databinding.ItemSchoolBinding
import com.chongre.jpmc.nycschools.model.School

class SchoolViewHolder(val binding: ItemSchoolBinding):
ViewHolder(binding.root) {
    fun bind(school: School) {
        binding.apply {
            tvSchoolName.text = school.schoolName
            val lastIndex = school.location.lastIndexOf("(")
            if(lastIndex!=-1) {
                tvLocation.text = school.location.substring(0, lastIndex-1)
            } else {
                tvLocation.text = school.location
            }

            tvPhoneNumber.text = school.phoneNumber

        }
    }
}