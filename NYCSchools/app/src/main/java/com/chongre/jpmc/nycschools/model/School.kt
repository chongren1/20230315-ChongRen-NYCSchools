package com.chongre.jpmc.nycschools.model

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName


data class School(
    @SerializedName("dbn")
    val dbn: String,

    @SerializedName("school_name")
    val schoolName: String,

    @SerializedName("location")
    val location: String,

    @SerializedName("phone_number")
    val phoneNumber: String,

    @SerializedName("school_email")
    val schoolEmail: String,

    @SerializedName("website")
    val website: String
): Parcelable {
    constructor(parcel: Parcel) : this(
        parcel.readString() ?: "",
        parcel.readString() ?: "",
        parcel.readString() ?: "",
        parcel.readString() ?: "",
        parcel.readString() ?: "",
        parcel.readString() ?: ""
    ) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(dbn)
        parcel.writeString(schoolName)
        parcel.writeString(location)
        parcel.writeString(phoneNumber)
        parcel.writeString(schoolEmail)
        parcel.writeString(website)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<School> {
        override fun createFromParcel(parcel: Parcel): School {
            return School(parcel)
        }

        override fun newArray(size: Int): Array<School?> {
            return arrayOfNulls(size)
        }
    }
}