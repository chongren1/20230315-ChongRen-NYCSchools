package com.chongre.jpmc.nycschools.view

import android.content.Context
import android.net.ConnectivityManager
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import com.chongre.jpmc.nycschools.R

open class BaseActivity: AppCompatActivity() {

    fun isNetworkAvailable(): Boolean {
        val connectivityManager =
            getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager?
        val activeNetworkInfo = connectivityManager?.activeNetwork


        return activeNetworkInfo != null
    }

    fun showNoInternetMessage() {
        AlertDialog.Builder(this).apply {
            setMessage(R.string.msg_no_internet)
            setPositiveButton(R.string.btn_ok) {
                dialog, which ->
                dialog.dismiss()
            }
            setCancelable(false)
        }.create().show()
    }
}