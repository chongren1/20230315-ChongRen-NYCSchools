package com.chongre.jpmc.nycschools.view

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View.GONE
import android.view.View.VISIBLE
import androidx.activity.viewModels
import com.chongre.jpmc.nycschools.R
import com.chongre.jpmc.nycschools.databinding.ActivityMainBinding
import com.chongre.jpmc.nycschools.model.Resource
import com.chongre.jpmc.nycschools.view.adapter.SchoolAdapter
import com.chongre.jpmc.nycschools.viewmodel.MainViewModel
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class MainActivity : BaseActivity() {

    val viewModel: MainViewModel by viewModels()
    lateinit var binding: ActivityMainBinding
    lateinit var adapter: SchoolAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        setupObservers()

        if(isNetworkAvailable()) {
            viewModel.getNycSchools()
        } else {
            showNoInternetMessage()
        }

        setupEvents()
    }

    private fun setupEvents() {
        binding.btnRetry.setOnClickListener {
            if(isNetworkAvailable()) {
                viewModel.getNycSchools()
            } else {
                showNoInternetMessage()
            }
        }
    }

    private fun setupObservers() {
        viewModel.schools.observe(this) {
            when(it) {
                is Resource.Loading -> {
                    binding.apply {
                        loadingGroup.visibility = VISIBLE
                        rvSchools.visibility = GONE
                        retryGroup.visibility = GONE
                    }
                }
                is Resource.Message -> {
                    binding.apply {
                        loadingGroup.visibility = GONE
                        rvSchools.visibility = GONE
                        retryGroup.visibility = VISIBLE
                        tvRetryMsg.text = it.msg
                    }
                }

                is Resource.Success -> {
                    adapter = SchoolAdapter(it.data)
                    binding.apply {
                        loadingGroup.visibility = GONE
                        rvSchools.visibility = VISIBLE
                        retryGroup.visibility = GONE
                        rvSchools.adapter = adapter

                        adapter.setOnSchoolSelectedListener {
                            val sIntent = Intent(baseContext, SchoolInfoActivity::class.java).apply {
                                putExtra("school", it)
                            }
                            startActivity(sIntent)
                        }
                    }
                }
            }
        }
    }
}