# 20230315-ChongRen-NYCSchools

## Language Used
- Kotlin

## Architecture Used
- MVVM with repository pattern

## Libraries Used
- Retrofit: For REST API calls
- Coroutine: For Asychronous Programming
- Dagger Hilt: Dependencies Injection
